<?php
include "config.php";

?>

<!DOCTYPE html>
<html lang="de-CH">
<head>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <title>Bestell-Portal</title>
    <link rel="icon" href="https://ng-design.ch/wp-content/uploads/2020/06/cropped-ngSquare-1-192x192.png">
    <link rel="stylesheet" type="text/css" href="style.css"/>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css"
          integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.21/css/jquery.dataTables.css">
</head>

<body>

<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <a class="navbar-brand" href="#"><h1>Bestell-Portal</h1></a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav"
            aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="ml-auto align-middle cartBtn" style="text-align: center">
        <i style="font-size: 40px;" class="fa fa-shopping-cart"></i>
        <p style="font-size: 15px; font-weight: bold">Warenkorb</p>
    </div>
</nav>

<div style="padding-top: 50px; padding-left: 200px; padding-right: 200px;">
    <div class="table-responsive" style="margin-bottom: 50px;">
        <table class="table" id="products">
            <thead>
            <tr>
                <th style="width: 75px;">ID</th>
                <th style="width: 200px;">Bezeichnung</th>
                <th style="width: 200px;">Bestand (KG)</th>
                <th style="width: 200px;">Bestellmenge</th>
                <th style="width: 10px;"></th>
            </tr>
            </thead>
            <?php
            $pdoQuery = "SELECT * FROM bestand";

            if (isset($pdodbcon)) {
                $pdoQuery_run = $pdodbcon->query($pdoQuery);
            }

            if ($pdoQuery_run) {
                foreach ($pdoQuery_run->fetchAll() as $result) {
                    echo '<tr style="height: 56px !important; vertical-align: middle">';
                    echo '<td> ' . $result['id'] . ' </td>';
                    echo '<td> ' . $result['bezeichnung'] . ' </td>';
                    echo '<td> ' . $result['bestand'] . ' </td>';
                    echo '<td><input type="number" id="Bestellmenge" placeholder="Bestellmenge" required="required" autofocus="autofocus" min="1" max="100" style="width: 190px;"/></td>';
                    echo '<td style="text-align: right;"><a href="https://www.google.com/"><i class="fa fa-cart-plus addToCart" style="font-size: 40px;"></i></a></td>';
                    echo '</tr>';
                }
            } else {
                echo '<script>alert("Keine Daten gefunden")</script>';
            }
            ?>
        </table>
    </div>

    <form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post" style="margin-bottom: 200px;">
        <input type="submit" name="order" value="Bestellen" class="buttons" style="float: right;">
        <input type="submit" name="display" value="Aktualisieren" class="buttons"
               style="float: right; margin-right: 15px;">
    </form>

</div>
</body>
</html>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.js"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js"
        integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN"
        crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"
        integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV"
        crossorigin="anonymous"></script>
<script>
    $(document).ready(function () {
        $('#products').DataTable();
    });
</script>